#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__

#include "cocos2d.h"

USING_NS_CC;

using namespace cocos2d;

class GameScene : public cocos2d::Layer
{
public:
	static cocos2d::Scene* createScene();

	virtual bool init();

	CREATE_FUNC(GameScene);

	void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event);
	void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event);
	bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event);

private:
	Sprite* m_ball;
	Sprite* m_paddle_player_1;
	Sprite* m_paddle_player_2;
	Sprite* m_edge;

	PhysicsWorld* m_world;

	void SetPhysicsWorld(cocos2d::PhysicsWorld *world) { m_world = world; };

};

#endif // __HELLOWORLD_SCENE_H__