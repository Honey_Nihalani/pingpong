#include "GameScene.h"

USING_NS_CC;

Scene* GameScene::createScene()
{
	auto scene = Scene::createWithPhysics();
	scene->getPhysicsWorld()->setDebugDrawMask(PhysicsWorld::DEBUGDRAW_ALL);
	scene->getPhysicsWorld()->setGravity(Vec2(0, 0));

	auto layer = GameScene::create();
	layer->SetPhysicsWorld(scene->getPhysicsWorld());	
	
	scene->addChild(layer);

	return scene;
}

bool GameScene::init()
{
	if (!Layer::init())
	{
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();


	auto edgeNode = Node::create();
	edgeNode->setPosition(Point(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));

	auto edgeBody = PhysicsBody::createEdgeBox(visibleSize, PhysicsMaterial (1.0f, 1.0f, 0), 3);
	edgeNode->setPhysicsBody(edgeBody);

	addChild(edgeNode);

	auto sprite = Sprite::create("CloseNormal.png");
	sprite->setPosition(Point(visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y));
	auto spriteBody = PhysicsBody::createCircle(sprite->getContentSize().width/2, PhysicsMaterial(1.0f, 1.0f, 0));
	Vect force = Vect(101000.0f, 101000.0f);
	spriteBody->applyImpulse(force);
	sprite->setPhysicsBody(spriteBody);

	addChild(sprite);

	auto node_player_1 = Node::create();
	node_player_1->setPosition(Point(visibleSize.width / 2 + origin.x, visibleSize.height * 0.1f + origin.y));

	auto rect_player_1 = PhysicsBody::createBox( Size(visibleSize.width * 0.2, visibleSize.width * 0.05) , PHYSICSBODY_MATERIAL_DEFAULT);
	rect_player_1->setDynamic(false);
	node_player_1->setPhysicsBody(rect_player_1);

	addChild(node_player_1);

	auto node_player_2 = Node::create();
	node_player_2->setPosition(Point(visibleSize.width / 2 + origin.x, visibleSize.height * 0.9f + origin.y));

	auto rect_player_2 = PhysicsBody::createBox(Size(visibleSize.width *0.2f, visibleSize.width *0.05f), PHYSICSBODY_MATERIAL_DEFAULT);
	rect_player_2->setDynamic(false);
	node_player_2->setPhysicsBody(rect_player_2);

	addChild(node_player_2);

	return true;
}

bool GameScene::onTouchBegan(cocos2d::Touch * touch, cocos2d::Event * event)
{
	return false;
}

void GameScene::onTouchMoved(cocos2d::Touch * touch, cocos2d::Event * event)
{
}

void GameScene::onTouchEnded(cocos2d::Touch * touch, cocos2d::Event * event)
{
}
